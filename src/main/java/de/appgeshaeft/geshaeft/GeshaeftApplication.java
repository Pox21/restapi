package de.appgeshaeft.geshaeft;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GeshaeftApplication {

    public static void main(String[] args) {
        SpringApplication.run(GeshaeftApplication.class, args);
    }

}
