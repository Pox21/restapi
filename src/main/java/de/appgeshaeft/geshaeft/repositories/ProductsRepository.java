package de.appgeshaeft.geshaeft.repositories;

import de.appgeshaeft.geshaeft.models.Product;

import java.util.List;

public interface ProductsRepository {
    List<Product> findAll();
}
