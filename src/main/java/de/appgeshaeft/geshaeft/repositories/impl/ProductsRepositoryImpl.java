package de.appgeshaeft.geshaeft.repositories.impl;

import de.appgeshaeft.geshaeft.models.Product;
import de.appgeshaeft.geshaeft.repositories.ProductsRepository;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

@Repository
public class ProductsRepositoryImpl implements ProductsRepository {

    private  final List<Product> products;

    public ProductsRepositoryImpl() {
        Product p1 = new Product(1L, "title 1", "descr 1");
        Product p2 = new Product(2L, "title 2", "descr 2");
        Product p3 = new Product(3L, "title 3", "descr 3");
        Product p4 = new Product(4L, "title 4", "descr 4");
        this.products = Arrays.asList(p1, p2, p3, p4);
    }
    @Override
    public List<Product> findAll() {
        return products;
    }
}


