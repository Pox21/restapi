package de.appgeshaeft.geshaeft.services.impl;

import de.appgeshaeft.geshaeft.models.Product;
import de.appgeshaeft.geshaeft.repositories.ProductsRepository;
import de.appgeshaeft.geshaeft.services.ProductsService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    public ProductsServiceImpl(ProductsRepository repository) {
        this.productsRepository = repository;
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }
}
