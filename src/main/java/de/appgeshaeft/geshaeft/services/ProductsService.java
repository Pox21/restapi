package de.appgeshaeft.geshaeft.services;

import de.appgeshaeft.geshaeft.models.Product;

import java.util.List;

public interface ProductsService {
    List<Product> getAllProducts();
}
