package de.appgeshaeft.geshaeft.controllers;

import de.appgeshaeft.geshaeft.models.Product;
import de.appgeshaeft.geshaeft.services.ProductsService;
import de.appgeshaeft.geshaeft.services.impl.ProductsServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ProductsController {
    private final ProductsService productsService;

    public ProductsController(ProductsService service) {
        this.productsService = service;
    }

    @GetMapping("/products")
    @ResponseBody
    public List<Product> getAllProducts() {
        return productsService.getAllProducts();
    }
}
